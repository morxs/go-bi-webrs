package modules

import (
	"encoding/json"
	"log"
	"net/http"
)

// TInfostore - hold infostore entries
var TInfostore InfoStore

// GetInfostore - Get infostore object
func GetInfostore(url *string) []InfostoreEntries {
	var nextURI string
	var TInfostoreEntries []InfostoreEntries

	// get first request and loop until next is nil
	TInfostoreEntries = reqInfoStore(url)

	nextURI = TInfostore.Next.Deferred.URI

	for nextURI != "" {
		TInfostore = InfoStore{}

		TInfostoreEntriesLoop := reqInfoStore(&nextURI)
		for _, e := range TInfostoreEntriesLoop {
			TInfostoreEntries = append(TInfostoreEntries, e)
		}

		nextURI = TInfostore.Next.Deferred.URI
	}
	return TInfostoreEntries
}

func reqInfoStore(url *string) []InfostoreEntries {
	if *url == "" {
		log.Fatal("Unable to run empty string")
	}
	req, err := http.NewRequest("GET", *url, nil)

	if err != nil {
		// handle err
		log.Fatal("Can't reach target URL")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-SAP-LogonToken", TToken.LogonToken)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
		log.Fatal("Can't authenticate. Either webservice is not run or unknown error")
	}

	if err := json.NewDecoder(resp.Body).Decode(&TInfostore); err != nil {
		log.Println(err)
	}

	var TInfostoreEntries []InfostoreEntries

	for _, e := range TInfostore.Entries {
		// dump to infostore entries object
		TInfostoreEntries = append(TInfostoreEntries, e)
		// fmt.Println("Item ", i)
		// fmt.Println("ID    : ", e.ID)
		// fmt.Println("Type  : ", e.Type)
		// fmt.Println("Name  : ", e.Name)
		// fmt.Println("CUID  : ", e.Cuid)
		// fmt.Println()
	}
	defer resp.Body.Close()

	return TInfostoreEntries
}
