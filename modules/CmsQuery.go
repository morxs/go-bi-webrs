package modules

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// TCmsQuery - hold the query input
var TCmsQuery CmsQuery

// TCmsFolder - hold CMS folder entries
var TCmsFolder CmsFolder

// GCmsFolderEntries - hold global output
var GCmsFolderEntries []CmsFolderEntries

// GetCMSFolder - Get folders using CMS Query
func GetCMSFolder(url *string) []CmsFolderEntries {

	// TCmsFolderEntries - hold CMS folder entries detail
	var TCmsFolderEntries []CmsFolderEntries

	if *url == "" {
		log.Fatal("Unable to run empty string")
	}

	CMSQueryFolder := "SELECT SI_ID, SI_NAME, SI_KIND, SI_PARENT_FOLDER, SI_CUID FROM CI_INFOOBJECTS WHERE SI_KIND = 'Folder' AND SI_PARENT_FOLDER in (%1)"

	log.Println("SATU")
	// get folder from other module
	// -- no need to check next, as it already catered
	TInfoStoreEntries := GetInfostore(&InfostoreAppURL)

	if len(TInfoStoreEntries) != 0 {
		// dump entries to global entries
		for _, e := range TInfoStoreEntries {
			var tempCmsFolderEntries CmsFolderEntries
			tempCmsFolderEntries.SIID, _ = strconv.Atoi(e.ID)
			tempCmsFolderEntries.SINAME = e.Name
			tempCmsFolderEntries.SICUID = e.Cuid
			tempCmsFolderEntries.SIKIND = e.Type
			tempCmsFolderEntries.SIPARENTFOLDER = 0
			GCmsFolderEntries = append(GCmsFolderEntries, tempCmsFolderEntries)
		}

		// ---------== build the payload == ----------

		// concatenate all ID for query parameter
		var buffer bytes.Buffer
		for i, e := range TInfoStoreEntries {
			buffer.WriteString("'" + e.ID + "'")
			if i != len(TInfoStoreEntries)-1 {
				buffer.WriteString(",")
			}
		}
		// log.Println(buffer.String()) // test string

		CMSQueryFolder = strings.Replace(CMSQueryFolder, "%1", buffer.String(), -1)

		// log.Println(CMSQueryFolder) // test string

		TCmsQuery.Query = CMSQueryFolder

		jQueryPayload, err := json.Marshal(TCmsQuery)
		if err != nil {
			log.Fatal("Error when decoding json")
		}

		// payload := strings.NewReader(string(jQueryPauload))

		// run query first time and fill in the entries to holder
		TCmsFolderEntries = reqCMSFolder(url, &jQueryPayload)

		// testing - BEGIN
		// TCmsFolderEntries = reqCMSFolder("http://wilbipas01.hec.wilmarapps.com:6405/biprws/v1/cmsquery?page=2&pagesize=50", payload)
		// payload = strings.NewReader(string(jQueryPayload))
		// TCmsFolderEntriesDuo := reqCMSFolder("http://wilbipas01.hec.wilmarapps.com:6405/biprws/v1/cmsquery?page=2&pagesize=50", payload)
		// for _, e := range TCmsFolderEntriesDuo {
		// 	TCmsFolderEntries = append(TCmsFolderEntries, e)
		// }
		// testing - END

		nextURI := TCmsFolder.Next.Deferred.URI

		for nextURI != "" {
			TCmsFolder = CmsFolder{}
			log.Println(nextURI)
			TCmsFolderEntriesLoop := reqCMSFolder(&nextURI, &jQueryPayload)
			for _, e := range TCmsFolderEntriesLoop {
				TCmsFolderEntries = append(TCmsFolderEntries, e)
			}
			log.Println("TIGA")
			nextURI = TCmsFolder.Next.Deferred.URI
		}
	}

	// dump to global variable
	for _, e := range TCmsFolderEntries {
		GCmsFolderEntries = append(GCmsFolderEntries, e)
	}

	return GCmsFolderEntries
}

// func reqCMSFolder(url string, _payload io.Reader) []CmsFolderEntries {
func reqCMSFolder(url *string, _payload *[]byte) []CmsFolderEntries {
	if *url == "" {
		log.Fatal("Unable to run empty string")
	}

	// this is needed due to once io.reader are read, it become empty string
	payload := strings.NewReader(string(*_payload))

	// you can also do payload.Reset() to reset the reader position

	if payload == nil {
		log.Fatal("Unable to continue with empty payload")
	}

	req1, err1 := http.NewRequest("POST", *url, payload)

	if err1 != nil {
		// handle err
		log.Fatal("Can't reach target URL")
	}
	req1.Header.Set("Accept", "application/json")
	req1.Header.Set("Content-Type", "application/json")
	req1.Header.Set("cache-control", "no-cache")
	req1.Header.Set("X-SAP-LogonToken", TToken.LogonToken)

	resp1, err1 := http.DefaultClient.Do(req1)
	if err1 != nil {
		// handle err
		log.Fatal("Can't authenticate. Either webservice is not run or unknown error")
	}

	if resp1.StatusCode != 200 {
		log.Printf("HEADER: %s\n", req1.Header)
		log.Printf("url: %s\n", *url)
		log.Println("payload: ", payload)
		log.Fatalf("Problem when running request: %d - %s", resp1.StatusCode, resp1.Status)
	}

	// var TCmsFolder CmsFolder
	if err1 := json.NewDecoder(resp1.Body).Decode(&TCmsFolder); err1 != nil {
		log.Println(err1)
	}

	var tCmsFolderEntries []CmsFolderEntries

	for _, e := range TCmsFolder.Entries {
		// dump to infostore entries object
		tCmsFolderEntries = append(tCmsFolderEntries, e)
		// fmt.Println("Item ", i)
		// fmt.Println("ID    : ", e.ID)
		// fmt.Println("Type  : ", e.Type)
		// fmt.Println("Name  : ", e.Name)
		// fmt.Println("CUID  : ", e.Cuid)
		// fmt.Println()
	}
	defer resp1.Body.Close()

	return tCmsFolderEntries
}
