package modules

// AuthToken - Result Token for you to authenticate BI Web API
type AuthToken struct {
	LogonToken string `json:"logonToken"`
}

// LoginPayload - Payload for login
type LoginPayload struct {
	UserName string `json:"userName"`
	Password string `json:"password"`
	Auth     string `json:"auth"`
}

// CmsQuery - Structure for CMS query input
type CmsQuery struct {
	Query string `json:"query"`
}

// CmsFolder - Structure for CMS Folder
type CmsFolder struct {
	Metadata Metadata           `json:"__metadata"`
	First    First              `json:"first"`
	Next     Next               `json:"next"`
	Last     Last               `json:"last"`
	Entries  []CmsFolderEntries `json:"entries"`
}

// CmsFolderEntries - Real object of CMS Folder
type CmsFolderEntries struct {
	SIID           int    `json:"SI_ID"`
	SINAME         string `json:"SI_NAME"`
	SICUID         string `json:"SI_CUID"`
	SIKIND         string `json:"SI_KIND"`
	SIPARENTFOLDER int    `json:"SI_PARENT_FOLDER"`
}

// Folder - struct for folder json reply
type Folder struct {
	Metadata Metadata        `json:"__metadata"`
	First    First           `json:"first"`
	Last     Last            `json:"last"`
	Next     Next            `json:"next"`
	Entries  []FolderEntries `json:"entries"`
}

// FolderEntries - Real object of infostore
type FolderEntries struct {
	Metadata    Metadata `json:"__metadata"`
	Cuid        string   `json:"cuid"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	ID          string   `json:"id"`
	Type        string   `json:"type"`
	Ownerid     string   `json:"ownerid"`
	Updated     string   `json:"updated"`
}

// InfoStore - struct for Infostore json reply
type InfoStore struct {
	Metadata Metadata           `json:"__metadata"`
	First    First              `json:"first"`
	Last     Last               `json:"last"`
	Next     Next               `json:"next"`
	Entries  []InfostoreEntries `json:"entries"`
}

// InfostoreEntries - Real object of infostore
type InfostoreEntries struct {
	Metadata    Metadata `json:"__metadata"`
	Cuid        string   `json:"cuid"`
	Name        string   `json:"name"`
	ID          string   `json:"id"`
	Type        string   `json:"type"`
	Description string   `json:"description,omitempty"`
}

// User - struct for user json reply
type User struct {
	Metadata Metadata      `json:"__metadata"`
	First    First         `json:"first"`
	Next     Next          `json:"next"`
	Last     Last          `json:"last"`
	Entries  []UserEntries `json:"entries"`
}

// UserEntries - Real object of User
type UserEntries struct {
	Metadata    Metadata `json:"__metadata"`
	Cuid        string   `json:"cuid"`
	Name        string   `json:"name"`
	Fullname    string   `json:"fullname"`
	Description string   `json:"description"`
	ID          string   `json:"id"`
	Type        string   `json:"type"`
	OwnerID     string   `json:"ownerid"`
	Updated     string   `json:"updated"`
	ParentID    string   `json:"parentid"`
}

// UserDetail - Struct for user detail json reply
type UserDetail struct {
	Entries []UserDetailEntries `json:"entries"`
}

// UserDetailEntries - real object of user detail
type UserDetailEntries struct {
	Cuid                          string `json:"cuid"`
	IsAccountDisabled             bool   `json:"isAccountDisabled"`
	IsPasswordToChangeAtNextLogon bool   `json:"isPasswordToChangeAtNextLogon"`
	IsPasswordChangeAllowed       bool   `json:"isPasswordChangeAllowed"`
	Description                   string `json:"description"`
	FullName                      string `json:"fullName"`
	IsPasswordExpiryAllowed       bool   `json:"isPasswordExpiryAllowed"`
	Title                         string `json:"title"`
	EmailAddress                  string `json:"emailAddress"`
	Usergroups                    string `json:"usergroups"`
	Connection                    int 	 `json:"connection"`
	ID                            int    `json:"id"`
	InboxID                       int    `json:"inboxID"`
}

// Usergroup - Struct for usergroup
type Usergroup struct {
	Metadata Metadata           `json:"__metadata"`
	First    First              `json:"first"`
	Next     Next               `json:"next"`
	Last     Last               `json:"last"`
	Entries  []UsergroupEntries `json:"entries"`
}

// UsergroupEntries - Real data for usergroup
type UsergroupEntries struct {
	Cuid        string `json:"cuid"`
	Keywords    string `json:"keywords"`
	Name        string `json:"name"`
	Description string `json:"description"`
	ID          string `json:"id"`
	Type        string `json:"type"`
	Updated     string `json:"updated"`
	Parentid    string `json:"parentid"`
}

// UsergroupDetail - struct for usergroup detail
type UsergroupDetail struct {
	Metadata    Metadata `json:"__metadata"`
	Cuid        string   `json:"cuid"`
	Keywords    string   `json:"keywords"`
	Parentcuid  string   `json:"parentcuid"`
	Created     string   `json:"created"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	ID          string   `json:"id"`
	Parentid    string   `json:"parentid"`
}

// Metadata - generic metadata
type Metadata struct {
	URI string `json:"uri"`
}

// Deferred - generic metadata
type Deferred struct {
	URI string `json:"uri"`
}

// First - generic metadata
type First struct {
	Deferred Deferred `json:"__deferred"`
}

// Last - generic metadata
type Last struct {
	Deferred Deferred `json:"__deferred"`
}

// Next - generic metadata
type Next struct {
	Deferred Deferred `json:"__deferred"`
}
