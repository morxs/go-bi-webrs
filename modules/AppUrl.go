package modules

// AuthAppURL - hold url for auth
var AuthAppURL string

// DeAuthAppURL - hold url for de-auth
var DeAuthAppURL string

// UserAppURL - Infostore app url
var UserAppURL string

// UserDetailAppURL - Infostore app url
var UserDetailAppURL string

// FolderAppURL - Infostore app url
var FolderAppURL string

// InfostoreAppURL - Infostore app url
var InfostoreAppURL string

// CMSAppURL - CMS query app url
var CMSAppURL string

// UserGroupAppURL - Usergroup app url
var UserGroupAppURL string

// UserGroupDetailAppURL - Usergroup app url
var UserGroupDetailAppURL string

// FormURL - generate app url
func FormURL() {
	AuthAppURL = BaseBOUrl + "/logon/long"
	DeAuthAppURL = BaseBOUrl + "/v1/logoff"

	FolderAppURL = BaseBOUrl + "/v1/folders"
	InfostoreAppURL = BaseBOUrl + "/infostore"

	CMSAppURL = BaseBOUrl + "/v1/cmsquery"

	UserAppURL = BaseBOUrl + "/v1/users"
	UserDetailAppURL = BaseBOUrl + "/users/"

	UserGroupAppURL = BaseBOUrl + "/v1/usergroups"
	UserGroupDetailAppURL = BaseBOUrl + "/v1/usergroups/"
}

// PrepDeAuthURL - Prepare de-auth app url
func PrepDeAuthURL() string {
	return DeAuthAppURL
}

// PrepAuthURL - Prepare auth app url
func PrepAuthURL() string {
	return AuthAppURL
}

// PrepUserAppURL - Prepare User url
func PrepUserAppURL() string {
	return UserAppURL
}

// PrepUserDetailAppURL - Prepare User Detail url
func PrepUserDetailAppURL(id string) string {
	return UserDetailAppURL + id
}

// PrepUserGroupDetailAppURL - Prepare Usergroup Detail url
func PrepUserGroupDetailAppURL(id string) string {
	return UserGroupDetailAppURL + id
}

// PrepFolderAppURL - Prepare Infostore url
func PrepFolderAppURL() string {
	return FolderAppURL
}

// PrepInfoStoreURL - Prepare Infostore url
func PrepInfoStoreURL() string {
	return InfostoreAppURL
}
