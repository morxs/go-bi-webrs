package modules

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// TFolder - hold infostore entries
var TFolder Folder

// GetFolder - Get infostore object
func GetFolder(url string) {

	// get first request and loop until next is nil
	reqFolder(url)

	for TFolder.Next.Deferred.URI != "" {
		reqFolder(TFolder.Next.Deferred.URI)
	}
}

func reqFolder(url string) {
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		// handle err
		log.Fatal("Can't reach target URL")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-SAP-LogonToken", TToken.LogonToken)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
		log.Fatal("Can't authenticate. Either webservice is not run or unknown error")
	}

	if err := json.NewDecoder(resp.Body).Decode(&TFolder); err != nil {
		log.Println(err)
	}

	fmt.Println(TFolder)

	for i, e := range TFolder.Entries {
		fmt.Println("Item ", i)
		fmt.Println("ID    : ", e.ID)
		fmt.Println("Type  : ", e.Type)
		fmt.Println("Name  : ", e.Name)
		fmt.Println("CUID  : ", e.Cuid)
		fmt.Println()
	}
	defer resp.Body.Close()
}
