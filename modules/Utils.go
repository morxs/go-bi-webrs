package modules

import (
	"log"
	"strings"
)

func splitBracketToItems(bracket *string) *[]string {
	if *bracket == "" {
		log.Println("Item empty")
	}
	// bracket = strings.Replace(bracket, "[", "", -1)
	// bracket = strings.Replace(bracket, "]", "", -1)
	*bracket = (*bracket)[1 : len(*bracket)-1]
	result := strings.Split(strings.Replace(*bracket, " ", "", -1), ",")
	//return &(strings.Split(strings.Replace(*bracket, " ", "", -1), ","))
	return &result
}
