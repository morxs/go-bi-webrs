package modules

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

// TToken - hold auth token
var TToken AuthToken

var tLoginPayload LoginPayload

var jPayload []byte

// GetToken - Get token from auth app url
func GetToken(url *string) {
	payload := strings.NewReader(string(jPayload))
	req, err := http.NewRequest("POST", *url, payload)

	if err != nil {
		// handle err
		log.Fatal("Can't reach target URL")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
		log.Fatal("Can't authenticate. Either webservice is not run or unknown error")
	}

	// fmt.Println(resp.Body)
	// json.Unmarshal([]resp.Body, &tToken)

	if err := json.NewDecoder(resp.Body).Decode(&TToken); err != nil {
		log.Println(err)
	}

	// fmt.Println(TToken.LogonToken)

	log.Println("Auth Status: ", resp.Status)

	defer resp.Body.Close()
}

// DeAuthToken - invalidate token
func DeAuthToken(url *string, token *string) {
	req, err := http.NewRequest("POST", *url, nil)

	if err != nil {
		// handle err
		log.Fatal("Can't reach target URL")
	}
	req.Header.Set("Accept", "application/json")
	//req.Header.Set("X-SAP-LogonToken", TToken.LogonToken)
	req.Header.Set("X-SAP-LogonToken", *token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
		log.Fatal("Can't authenticate. Either webservice is not run or unknown error")
	}

	log.Println("DeAuth Status: ", resp.Status)
}
