package modules

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// TUserGroup - hold usergroup entries
var TUserGroup Usergroup

// TUsergroupDetail - hold usergroup entry
var TUsergroupDetail UsergroupDetail

// GetUsergroup - Get infostore object
func GetUsergroup(url string) {
	var nextURI string

	// get first request and loop until next is nil
	reqUser(url)
	nextURI = TUserGroup.Next.Deferred.URI

	fmt.Println(nextURI)
	for nextURI != "" {
		TUserGroup = Usergroup{} // this is how to initialize or clear struct
		reqUser(nextURI)
		nextURI = TUserGroup.Next.Deferred.URI
	}
}

func reqUsergroup(url string) {
	if url == "" {
		log.Fatal("Unable to run empty string")
	}

	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		// handle err
		log.Fatal("Can't reach target URL")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-SAP-LogonToken", TToken.LogonToken)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
		log.Fatal("Can't authenticate. Either webservice is not run or unknown error")
	}

	if err := json.NewDecoder(resp.Body).Decode(&TUserGroup); err != nil {
		log.Println(err)
	}

	// fmt.Println(TUserGroup)

	for i, e := range TUserGroup.Entries {
		fmt.Println("Item ", i)
		fmt.Println("CUID  : ", e.Cuid)
		fmt.Println("ID    : ", e.ID)
		fmt.Println("Type  : ", e.Type)
		fmt.Println("Name  : ", e.Name)
		fmt.Println("Desc  : ", e.Description)
		fmt.Println("Parent: ", e.Parentid)
		fmt.Println()
		GetUserDetail(PrepUserDetailAppURL(e.ID))
	}
	defer resp.Body.Close()
}

// GetUsergroupDetail - Get single usergroup detail
func GetUsergroupDetail(url string) {
	if url == "" {
		log.Fatal("Unable to run empty string")
	}

	req1, err1 := http.NewRequest("GET", url, nil)

	if err1 != nil {
		// handle err
		log.Fatal("Can't reach target URL")
	}
	req1.Header.Set("Accept", "application/json")
	req1.Header.Set("Content-Type", "application/json")
	req1.Header.Set("X-SAP-LogonToken", TToken.LogonToken)

	resp1, err1 := http.DefaultClient.Do(req1)
	if err1 != nil {
		// handle err
		log.Fatal("Can't authenticate. Either webservice is not run or unknown error")
	}

	if err1 := json.NewDecoder(resp1.Body).Decode(&TUsergroupDetail); err1 != nil {
		log.Println(err1)
	}

	// fmt.Println(TUsergroupDetail)
	// fmt.Println(">>CUID      : ", TUsergroupDetail.Cuid)
	// fmt.Println(">>ID        : ", TUsergroupDetail.ID)
	// fmt.Println(">>ParentID  : ", TUsergroupDetail.Parentcuid)
	// fmt.Println(">>Name      : ", TUsergroupDetail.Name)
	// fmt.Println(">>Desc      : ", TUsergroupDetail.Description)
	// fmt.Println()

	defer resp1.Body.Close()
}
