package modules

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// TUser - hold user entries
var TUser User

// TUserDetail - hold user detail entries
var TUserDetail UserDetail

// GetUser - Get infostore object
func GetUser(url string) {
	var nextURI string

	// get first request and loop until next is nil
	reqUser(url)
	nextURI = TUser.Next.Deferred.URI

	fmt.Println(nextURI)
	for nextURI != "" {
		TUser = User{} // this is how to initialize or clear struct
		reqUser(nextURI)
		nextURI = TUser.Next.Deferred.URI
	}
}

func reqUser(url string) {
	if url == "" {
		log.Fatal("Unable to run empty string")
	}

	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		// handle err
		log.Fatal("Can't reach target URL")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-SAP-LogonToken", TToken.LogonToken)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
		log.Fatal("Can't authenticate. Either webservice is not run or unknown error")
	}

	if resp.StatusCode != 200 {
		log.Fatalf("Problem when running request: %d - %s", resp.StatusCode, resp.Status)
	}

	if err := json.NewDecoder(resp.Body).Decode(&TUser); err != nil {
		log.Println(err)
	}

	// fmt.Println(TUser)

	for i, e := range TUser.Entries {
		fmt.Println("Item ", i)
		fmt.Println("Meta  : ", e.Metadata.URI)
		fmt.Println("ID    : ", e.ID)
		fmt.Println("Type  : ", e.Type)
		fmt.Println("Name  : ", e.Name)
		fmt.Println("Full  : ", e.Fullname)
		fmt.Println("Desc  : ", e.Description)
		fmt.Println("CUID  : ", e.Cuid)
		fmt.Println()
		GetUserDetail(PrepUserDetailAppURL(e.ID))
	}
	defer resp.Body.Close()
}

// GetUserDetail - Get User's detail
func GetUserDetail(url string) {
	if url == "" {
		log.Fatal("Unable to run empty string")
	}

	req1, err1 := http.NewRequest("GET", url, nil)

	if err1 != nil {
		// handle err
		log.Fatal("Can't reach target URL")
	}
	req1.Header.Set("Accept", "application/json")
	req1.Header.Set("Content-Type", "application/json")
	req1.Header.Set("X-SAP-LogonToken", TToken.LogonToken)

	resp1, err1 := http.DefaultClient.Do(req1)
	if err1 != nil {
		// handle err
		log.Fatal("Can't authenticate. Either webservice is not run or unknown error")
	}

	if resp1.StatusCode != 200 {
		log.Fatalf("Problem when running request: %d - %s", resp1.StatusCode, resp1.Status)
	}

	if err1 := json.NewDecoder(resp1.Body).Decode(&TUserDetail); err1 != nil {
		log.Println(err1)
	}

	// fmt.Println(TUserDetail)

	for _, e := range TUserDetail.Entries {
		fmt.Println(">>CUID      : ", e.Cuid)
		fmt.Println(">>ID        : ", e.ID)
		fmt.Println(">>Disabled  : ", e.IsAccountDisabled)
		fmt.Println(">>Email     : ", e.EmailAddress)
		fmt.Println(">>Usergroup : ", e.Usergroups)
		slUsergroups := splitBracketToItems(&e.Usergroups)
		for i, e := range *slUsergroups {
			// fmt.Println(PrepUserGroupDetailAppURL(e))
			GetUsergroupDetail(PrepUserGroupDetailAppURL(e))
			fmt.Println(i, ": ", e, "(", TUsergroupDetail.Name, ")")
		}
		fmt.Println()
	}
	defer resp1.Body.Close()
}
