package modules

import (
	"encoding/json"
	"log"

	"github.com/go-ini/ini"
)

// BaseBOUrl - Base url for BO
var BaseBOUrl string

// PrepConfig - Read config and prepare all the things
func PrepConfig(fileName string) {
	iniCfg, err := ini.Load(fileName)
	if err != nil {
		log.Printf("Reading config...")
		log.Fatal(err)
	}
	iniSection := iniCfg.Section("server")
	iniKeyUsername := iniSection.Key("uid").String()
	iniKeyPassword := iniSection.Key("pwd").String()
	iniKeyHost := iniSection.Key("host").String()
	iniKeyPort := iniSection.Key("port").String()
	iniKeyApp := iniSection.Key("app").String()

	// TODO: sanitize Host and App, remove trailing /

	// dont add trailing /
	BaseBOUrl = "http://" + iniKeyHost + ":" + iniKeyPort + "/" + iniKeyApp
	// fmt.Println(iniKeyUsername + iniKeyPassword)
	// payload := strings.NewReader("{\"userName\":\"bosupport01\",\"password\":\"Wilmar@2020\",\"auth\":\"secEnterprise\"}")
	tLoginPayload.UserName = iniKeyUsername
	tLoginPayload.Password = iniKeyPassword
	tLoginPayload.Auth = "secEnterprise"
	jPayload, err = json.Marshal(tLoginPayload)
	if err != nil {
		log.Fatal("Error when decoding json")
	}
}
