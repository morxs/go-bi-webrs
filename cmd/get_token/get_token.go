package main

import (
	"fmt"

	"bitbucket.org/morxs/go-bi-webrs/modules"
)

func main() {
	var sCfg string

	sCfg = "config.cfg"

	modules.PrepConfig(sCfg)

	modules.FormURL()

	modules.GetToken(&modules.AuthAppURL)

	fmt.Println(modules.TToken.LogonToken)
}
