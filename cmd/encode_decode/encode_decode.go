package main

import (
	"fmt"

	"bitbucket.org/morxs/go-bi-webrs/modules"
)


func main() {
	var sCfg string

	sCfg = "config.cfg"

	modules.PrepConfig(sCfg)
	fmt.Println(string(modules.jPayload))
	fmt.Println(modules.BaseBOUrl)
	fmt.Println(modules.PrepAuthURL())
}
