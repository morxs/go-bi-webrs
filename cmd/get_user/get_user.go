package main

import (
	"bitbucket.org/morxs/go-bi-webrs/modules"
	"fmt"
)

func main() {
	var sCfg string

	sCfg = "config.cfg"

	modules.PrepConfig(sCfg)

	modules.FormURL()

	modules.GetToken(&modules.AuthAppURL)

	fmt.Println(modules.TToken.LogonToken)

	modules.UserAppURL = modules.PrepUserAppURL()

	modules.GetUser(modules.UserAppURL)

	modules.DeAuthToken(&modules.DeAuthAppURL, &modules.TToken.LogonToken)
}
