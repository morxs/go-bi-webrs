package main

import (
	"fmt"
	"log"

	"bitbucket.org/morxs/go-bi-webrs/modules"
)

func main() {
	var sCfg string

	sCfg = "config.cfg"

	modules.PrepConfig(sCfg)

	modules.FormURL()

	modules.GetToken(&modules.AuthAppURL)

	log.Println(modules.TToken.LogonToken)

	z := modules.GetCMSFolder(&modules.CMSAppURL)

	for _, e := range z {
		fmt.Printf("%d, %s, %s, %s, %d\n", e.SIID, e.SINAME, e.SICUID, e.SIKIND, e.SIPARENTFOLDER)
	}

	modules.DeAuthToken(&modules.DeAuthAppURL, &modules.TToken.LogonToken)
}
