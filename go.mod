module bitbucket.org/morxs/go-bi-webrs

go 1.12

require (
	github.com/go-ini/ini v1.46.0
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	gopkg.in/ini.v1 v1.46.0 // indirect
)
